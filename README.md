<div align="center">
  <img src="vso-logo.svg" height="120">
  <h1 align="center">Vanilla System Operator</h1>
  <p align="center">VSO is an utility which allows you to perform maintenance tasks on your Vanilla OS installation.</p>
</div>

<br/>

## Help

```
Usage: vso [options] [command] [arguments]

Options:
  -h, --help            Show this help message and exit

Commands:
    config              Configure VSO
    developer-program   Join the developers program
    help                Show this help message and exit
    trigger-update          Force a system update
    version             Show version and exit
```
